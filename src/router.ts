import { createWebHistory, createRouter } from "vue-router";
import { RouteRecordRaw } from "vue-router";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    alias: "/product-list",
    name: "productList",
    component: () => import("@/pages/Product.vue"),
  },
  {
    path: "/product/:id",
    name: "productDetail",
    component: () => import("./pages/Detail.vue"),
  },
  {
    path: "/add",
    name: "add",
    component: () => import("@/pages/AddTutorial.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
